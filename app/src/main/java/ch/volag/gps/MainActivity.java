package ch.volag.gps;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

public class MainActivity extends AppCompatActivity {
    private LocationManager locationManager;
    private String provider;
    private int time;
    private boolean bFound;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 118;

    private FusedLocationProviderClient mFusedLocationClient;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);


    @BindView(R.id.tv_lat)
    TextView mLat;
    @BindView(R.id.tv_long)
    TextView mLong;
    @BindView(R.id.tv_time_too)
    TextView mTimeTo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mLat.setText("Location not available");
        mLong.setText("Location not available");
    }

    //Start Scheduler for 1 hour ask for poitin
    public void checkPosForAnHour() {

        final Runnable beeper = new Runnable() {
            public void run() {
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
                checkPosition();
            }
        };

        final ScheduledFuture<?> beeperHandle = scheduler.scheduleAtFixedRate(beeper, 10, 500, MILLISECONDS);
        scheduler.schedule(new Runnable() {
            public void run() { beeperHandle.cancel(true); }
        }, 60 * 60, SECONDS);
    }

    //Check Position every Scheduler FixRate
    public void checkPosition() {
        if (!checkLocationPermission()) {
            requestPermissions();
        } else {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {

                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.

                    if (!bFound) {
                        time+=500;
                        mTimeTo.setText("Time:" + time/1000);
                    }

                    if (location != null) {
                        mLat.setText(location.getLatitude() + "");
                        mLong.setText(location.getLongitude() + "" );
                        bFound = true;
                        mTimeTo.setText("Time need to found:" + time + " mSec");
                    }
                }
            });
        }
    }

    //OnCkick button start
    @OnClick(R.id.btn_start) void start(){
        time = 0;
        mTimeTo.setText("");
        checkPosForAnHour();
    }

    //Check of Permission and ask for
    @Override
    public void onStart() {
        super.onStart();

        if (!checkLocationPermission()) {
            requestPermissions();
        }
    }

    //Ask if the Permisson set for the app
    public boolean checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    //Ask for Permission
    //ACCESS_FINE_LOCATION
    public void requestPermissions(){
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
    }

    //The answer of the request of the Permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
                } else {
                    // Permission Denied
                    Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}